import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context';
import { HomeScreen, HotScreen, WriteScreen, PersonScreen,Detail, ClassifyPage } from './source/screen/index'
import { BottomIcon } from './source/components/index'
const Tab = createBottomTabNavigator();

function TabScreen() {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;
          if (route.name === 'Home') {
            iconName = 'homeAct'
          } else if (route.name === 'Hot') {
            iconName = 'HotAct'
          } else if (route.name === 'Write') {
            iconName = 'writeAct'
          } else {
            iconName = 'personAct'
          }
          // You can return any component that you like here!
          return <BottomIcon name={iconName} />;
        }
      })}
    >
      <Tab.Screen name="Home" component={HomeScreen} options={{
        headerStyle: {
          height:0
        }
      }} />
      <Tab.Screen name='Hot' component={HotScreen} options={{
        headerStyle: {
          height:0
        }
      }} />
      <Tab.Screen name='Write' component={WriteScreen} options={{
        headerStyle: {
          height:0
        }
      }} />

      <Tab.Screen name='Person' component={PersonScreen} options={{
        headerStyle: {
          height:0
        }
      }}  />
    </Tab.Navigator>
  )
}
const Stack = createNativeStackNavigator();
// 底部按钮导航
// 堆栈导航嵌套tab导航
export default function App() {
  return (
    <SafeAreaProvider>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="HomeTab">
          <Stack.Screen name="HomeTab" component={TabScreen} />
          <Stack.Screen name='WriteTab' component={TabScreen} />
          <Stack.Screen name='HotTab' component={TabScreen} />
          <Stack.Screen name='PersonTab' component={TabScreen} />
          <Stack.Screen name="Details" component={Detail} />
          <Stack.Screen name="Classify" component={ClassifyPage} />
        </Stack.Navigator>
      </NavigationContainer>
    </SafeAreaProvider>
  );
}

