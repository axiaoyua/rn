import { View, Text, TouchableOpacity, FlatList, StyleSheet, Image, Button } from 'react-native'
import { observer } from 'mobx-react';
import { blogModel } from './../../store/store'
import { useEffect, useState } from 'react'
import {ListModel} from './../components/list'

export const ClassifyPage = observer((props) => {
    useEffect(() => {
        blogModel.classifyList(props.route.params)
        // console.log("组件挂载 ");
    }, [])

    const goDetail = (item) => {
        item.count++
        blogModel.updataClick(item.count, item._id, "count")
        props.navigation.navigate('Details', {
            id: item._id,
            from:"classifyList"
        })
    }

    const beLick = (item, type) => {
        if (type == 'good') {
            item.good++
            blogModel.updataClick(item.good, item._id, type)
        } else {
            item.bad++
            blogModel.updataClick(item.bad, item._id, type)
        }
    }

    return (
        <View>
            <ListModel dataArr={blogModel.classifyListArr} goDetailAct={(item)=>goDetail(item)} beLikeAct={(item,type)=>beLick(item,type)}></ListModel>
        </View>
    )
})
