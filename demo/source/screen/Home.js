import {View,Text,FlatList,Image,Button} from "react-native"
import {HomeHead} from '../components/index'
import { blogModel } from './../../store/store'
import { observer } from 'mobx-react';
const DATA=[
    {key: 'Devin'},
    {key: 'Dan'},
    {key: 'Dominic'},
    {key: 'Jackson'},
    {key: 'James'},
    {key: 'Joel'},
    {key: 'John'},
    {key: 'Jillian'},
    {key: 'Jimmy'},
    {key: 'Julie'},
    {key:"aaa"},
    {key:"bbb"},
    {key:"ccc"},
    {key:"ddd"},
    {key:"eee"},
    {key:"fff"},
    {key:"ggg"},
    {key:"hhh"},
    {key:"iii"},
    {key:"yy"},
    {key:"kkk"},
    {key:"mm"},
]

export const  HomeScreen=observer((props)=>{
    let {navigation}=props
    return (
        <View style={{flex:1}}>
            <Text>Home!</Text>
            <FlatList
                data={DATA}
                ListHeaderComponent={HomeHead}
                renderItem={({item})=><Text style={{height:40,fontSize:30}}>{item.key}</Text>}
                style={{flex:1}}
            ></FlatList>
        </View>
    )
})