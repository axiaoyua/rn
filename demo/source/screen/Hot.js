import { View, Text, FlatList, StyleSheet, TouchableOpacity, Image, ScrollView } from "react-native"
import { observer } from "mobx-react"
import { blogModel } from './../../store/store'
import { useEffect } from "react"
import {ListModel} from './../components/list'
export const HotScreen = observer((props) => {
    useEffect(() => {
        blogModel.sortBlogs()
    }, [])

    useEffect(() => {
        blogModel.sortBlogs()
    }, [blogModel.blogsNumber])

    const goDetail = (item) => {
        item.count++
        blogModel.updataHotClick(item.count, item._id, "count")
        props.navigation.navigate('Details', {
            id: item._id,
            from: "Hot"
        })
    }

    const beLick = (item, type) => {
        if (type == 'good') {
            item.good++
            blogModel.updataHotClick(item.good, item._id, type)
        } else {
            item.bad++
            blogModel.updataHotClick(item.bad, item._id, type)
        }
    }

    const refreshAction = () => {
        blogModel.sortBlogs()
    }
    return (
        <View style={{ flex: 1 }}>

            <View style={{ height: 30 }}>
                <Text>Hot!</Text>
                <TouchableOpacity onPress={() => refreshAction()}>
                    <Image source={require('../imgs/refresh.png')} style={styles.refreshImgs}></Image>
                </TouchableOpacity>
            </View>
            <ListModel dataArr={blogModel.sortBlogsArr} goDetailAct={(item)=>goDetail(item)} beLikeAct={(item,type)=>beLick(item,type)}></ListModel>
        </View>
    )
})

const styles = StyleSheet.create({
    refreshImgs: {
        width: 25,
        height: 25,
        position: "absolute",
        right: 20,
        top: -15
    }
})