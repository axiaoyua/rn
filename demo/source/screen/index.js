import {HomeScreen} from './Home'
import {PersonScreen} from './Person'
import {WriteScreen} from './Write'
import {HotScreen} from './Hot'
import {Detail} from './Detail'
import {ClassifyPage} from './classifyList'

export {
    HomeScreen,
    PersonScreen,
    WriteScreen,
    HotScreen,
    Detail,
    ClassifyPage
}