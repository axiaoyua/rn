import { View, Text, StyleSheet, Image,
     TouchableOpacity, Button, FlatList,
      TextInput ,useWindowDimensions
      , Modal, Alert,TouchableHighlight
    } from 'react-native'
import { blogModel } from './../../store/store'
import { useEffect, useLayoutEffect, useState } from 'react';
import { observer } from 'mobx-react';

export const Detail = observer((props) => {
    const [contentValue,setContentValue]=useState('')
    const windowWidth = useWindowDimensions().width
    const contentWidth = windowWidth / 5 * 4

    // 弹窗
    const [modalVisible, setModalVisible] = useState(false);
    const [anothermodalVisible, setAnotherModalVisible] = useState(false);

    if (props.route.params.from == 'classifyList') {
        var i = blogModel.pageDetail(props.route.params.id, 'classifyList')
        var arr = JSON.parse(JSON.stringify(blogModel.classifyListArr))
    } else if (props.route.params.from == 'Hot') {
        var i = blogModel.pageDetail(props.route.params.id, 'Hot')
        var arr = JSON.parse(JSON.stringify(blogModel.sortBlogsArr))
    } else {
        var i = blogModel.pageDetail(props.route.params.id, 'Write')
        var arr = JSON.parse(JSON.stringify(blogModel.newBlogs))
    }

    const page = arr[i]

    // 评论逻辑
    useEffect(() => {
        blogModel.getComment(page._id)
    }, [])


    const clickAction = (type) => {
        if (props.route.params.from == 'classifyList') {
            if (type == 'good') {
                page.good++
                blogModel.updataClick(page.good, page._id, 'good')
            } else {
                page.bad++
                blogModel.updataClick(page.bad, page._id, 'bad')
            }
        } else if (props.route.params.from == 'Hot') {
            if (type == 'good') {
                page.good++
                blogModel.updataHotClick(page.good, page._id, 'good')
            } else {
                page.bad++
                blogModel.updataHotClick(page.bad, page._id, 'bad')
            }
        } else {
            if (type == 'good') {
                page.good++
                blogModel.updataWriteClick(page.good, page._id, page.time, 'good')
            } else {
                page.bad++
                blogModel.updataWriteClick(page.bad, page._id, page.time, 'bad')
            }
        }

    }
    const pressAction=(contentValue)=>{
        if(contentValue==''){
            setAnotherModalVisible(true)
        }else{
            blogModel.addComment(contentValue,page.title,page._id)
            setContentValue('')
            setModalVisible(true)
        }
    }
    const _renderItem = ({ item }) => {
        return (
            <View style={{marginTop:10,marginBottom:30}}>
                <Text style={{fontSize:20,fontWeight:600}}>{item.content}</Text>
                <Text>{item.time}</Text>
            </View>
        )
    }
    const showInputs=()=>{
        return(
            <View style={{marginTop:10,marginBottom:30,alignItems: "center"}}>
                <TextInput
                    value={contentValue}
                    onChangeText={(text) => setContentValue(text)}
                    placeholder="说点什么······"
                    style={{ width: contentWidth,height:200 }}
                />
                <TouchableOpacity
                        style={styles.subBtn}
                        onPress={() => pressAction( contentValue)}>
                        <Text style={styles.btnText}>提交</Text>
                    </TouchableOpacity>
            </View>
        )
    }
    return (
        <View>
             {/* 弹窗1 */}
             <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                    setModalVisible(!modalVisible);
                }}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Text>评论添加成功··待审核</Text>
                        <TouchableHighlight
                            onPress={() => {
                                setModalVisible(!modalVisible);
                            }}
                            style={styles.subBtn}
                        >
                            <Text style={styles.btnText}>确定</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </Modal>


            {/* 弹窗2 */}
            <Modal
                animationType="slide"
                transparent={true}
                visible={anothermodalVisible}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                    setAnotherModalVisible(!anothermodalVisible);
                }}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Text>内容不能为空</Text>
                        <TouchableHighlight
                            onPress={() => {
                                setAnotherModalVisible(!anothermodalVisible);
                            }}
                            style={styles.subBtn}
                        >
                            <Text style={styles.btnText}>确定</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </Modal>

            <Text>Details!!!</Text>
            <View>
                <Text style={styles.title}>{page.title}</Text>
                <Text style={styles.content}>{page.content}</Text>

                <View style={styles.foot}>
                    <TouchableOpacity style={styles.btn} onPress={() => clickAction('good')}>
                        <Image source={require('../imgs/good.png')} style={styles.imgs}></Image>
                        <Text style={styles.countsNum}>{page.good}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btn} onPress={() => clickAction('bad')}>
                        <Image source={require('../imgs/bad.png')} style={styles.imgs}></Image>
                        <Text style={styles.countsNum}>{page.bad}</Text>
                    </TouchableOpacity>

                    <Image source={require('../imgs/count.png')} style={styles.imgs}></Image>
                    <Text style={styles.countsNum}>{page.count}</Text>
                </View>

                <Text style={styles.bottomText}>分类：{page.classify}</Text>
                <Text style={styles.time}>时间：{page.time}</Text>
            </View>

            <View>
                <Text style={{fontSize:20}}>添加评论</Text>
                {showInputs()}
            </View>
            
            {
                JSON.parse(JSON.stringify(blogModel.showCommentArr)).length==0?
                <View>
                    <Text style={{marginBottom:30}}>暂时还没有评论哦~</Text>
                </View>:
                <View>
                <Text style={{fontSize:20}}>评论展示</Text>
                <FlatList
                    data={JSON.parse(JSON.stringify(blogModel.showCommentArr))}
                    renderItem={_renderItem}
                />
            </View>
            }
            
        </View>
    )
})

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
      },
      modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
      },
    title: {
        fontSize: 30
    },
    content: {
        fontSize: 20
    },
    imgs: {
        width: 40,
        height: 40
    },
    foot: {
        marginLeft: 15,
        marginTop: 5,
        flexDirection: "row"
    },
    btn: {
        marginRight: 25,
        flexDirection: "row"
    },
    countsNum: {
        marginRight: 25,
        textAlign: "center",
        lineHeight: 40,
        fontSize: 15
    },
    bottomText: {
        marginTop: 5,
        fontSize: 20
    },
    time: {
        marginBottom: 40,
    },
    subBtn: {
        width: 60,
        height: 30,
        backgroundColor: "grey"
    },
    btnText: {
        textAlign: "center",
        lineHeight: 30
    }
})