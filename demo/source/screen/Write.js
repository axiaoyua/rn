import {
    View, Text, TextInput, StyleSheet, useWindowDimensions, TouchableOpacity, FlatList
    , Image,
    ScrollView, Modal, Alert, TouchableHighlight
} from "react-native"
import { observer } from "mobx-react"
import { blogModel } from './../../store/store'
import { useEffect, useState } from "react"
import {ListModel} from './../components/list'

export const WriteScreen = observer((props) => {

    // 文本输入框
    const [titleValue, changeTitleVale] = useState('')
    const [contentValue, changeContentVale] = useState('')
    const [classifyValue, changeClassifyVale] = useState('')

    // 弹窗
    const [modalVisible, setModalVisible] = useState(false);
    const [anothermodalVisible, setAnotherModalVisible] = useState(false);

    const windowWidth = useWindowDimensions().width
    const titleWidth = windowWidth / 5
    const contentWidth = windowWidth / 5 * 4
    const classifyWidth = windowWidth / 5

    const submitAction = (title, content, classify) => {
        blogModel.addBlog(title, content, classify)
        changeTitleVale('')
        changeContentVale('')
        changeClassifyVale('')

        // 弹窗
        setModalVisible(true)
    }

    // 提交按钮有两种情况：1.输入框内容为空，2.不为空
    // 文本输入框内容为空时不能点提交按钮
    const pressAction = (titleValue, contentValue, classifyValue) => {
        if (titleValue == '' || contentValue == '' || classifyValue == '') {
            setAnotherModalVisible(true)
        } else {
            submitAction(titleValue, contentValue, classifyValue)
        }
    }

    const goDetail = (item) => {
        item.count++

        blogModel.updataWriteClick(item.count, item._id, item.time, "count")

        props.navigation.navigate('Details', {
            id: item._id,
            from: "Write"
        })
    }
    const beLick = (item, type) => {
        if (type == 'good') {
            item.good++
            blogModel.updataWriteClick(item.good, item._id, item.time, type)
        } else {
            item.bad++
            blogModel.updataWriteClick(item.bad, item._id, item.time, type)
        }
    }


    return (
        <View style={{ flex: 1 }}>

            {/* 弹窗1 */}
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                    setModalVisible(!modalVisible);
                }}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Text>文章添加成功</Text>
                        <TouchableHighlight
                            onPress={() => {
                                setModalVisible(!modalVisible);
                            }}
                            style={styles.subBtn}
                        >
                            <Text style={styles.btnText}>确定</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </Modal>


            {/* 弹窗2 */}
            <Modal
                animationType="slide"
                transparent={true}
                visible={anothermodalVisible}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                    setAnotherModalVisible(!anothermodalVisible);
                }}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Text>内容不能为空</Text>
                        <TouchableHighlight
                            onPress={() => {
                                setAnotherModalVisible(!anothermodalVisible);
                            }}
                            style={styles.subBtn}
                        >
                            <Text style={styles.btnText}>确定</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </Modal>


            <ScrollView style={{ flex: 1 }}>
                <Text>Write!</Text>
                <View style={styles.inputWrap}>
                    <TextInput
                        value={titleValue}
                        onChangeText={(text) => changeTitleVale(text)}
                        placeholder="请输入标题"
                        style={[styles.input, { width: contentWidth }]}

                    />
                    <TextInput
                        value={contentValue}
                        onChangeText={(text) => changeContentVale(text)}
                        placeholder="请输入内容"
                        style={[styles.contentInput, { width: contentWidth }]}
                    />
                    <TextInput
                        value={classifyValue}
                        onChangeText={(text) => changeClassifyVale(text)}
                        placeholder="请输入分类"
                        style={[styles.input, { width: contentWidth }]}
                    />
                    <TouchableOpacity
                        style={styles.subBtn}
                        onPress={() => pressAction(titleValue, contentValue, classifyValue)}>
                        <Text style={styles.btnText}>提交</Text>
                    </TouchableOpacity>
                </View>
                <ListModel dataArr={blogModel.newBlogs} goDetailAct={(item)=>goDetail(item)} beLikeAct={(item,type)=>beLick(item,type)}></ListModel>
            </ScrollView>
        </View>
    )
})
const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    inputWrap: {
        alignItems: "center"
    },
    input: {
        height: 40,
    },
    contentInput: {
        height: 200
    },
    subBtn: {
        width: 60,
        height: 30,
        backgroundColor: "grey"
    },
    btnText: {
        textAlign: "center",
        lineHeight: 30
    }
})