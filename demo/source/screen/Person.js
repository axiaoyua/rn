import { View, Text, FlatList, TouchableOpacity ,StyleSheet} from "react-native"
import { observer } from 'mobx-react';
import { blogModel } from './../../store/store'
import { useEffect, useState } from "react";

export const PersonScreen = observer((props) => {
    useEffect(() => {
        blogModel.getAllComments()  
    }, [blogModel.commentsNumber])

    useEffect(() => {
        blogModel.getAllComments()
    }, [])

    const pressAction=(id,type)=>{
        blogModel.checkComment(id,type)
    }
    const _renderItem = ({ item }) => {
        return (
            <View style={{ marginTop: 10, marginBottom: 30 }}>
                <Text style={{ fontSize: 16, marginBottom: 5 }}>文章标题：{item.articleTitle}</Text>
                <Text style={{ fontSize: 16, marginBottom: 5 }}>评论内容：</Text>
                <Text style={{ fontWeight: 600 }}>{item.content}</Text>
                <Text>{item.time}</Text>
                {
                    item.state == 0 ?
                        <View style={styles.btnWrap}>
                            <TouchableOpacity style={styles.btn} onPress={()=>pressAction(item._id,"pass")}>
                                <Text style={styles.btnText}>通过</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.btn} onPress={()=>pressAction(item._id,"nopass")}>
                                <Text style={styles.btnText}>不通过</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.btn} onPress={()=>pressAction(item._id,"delete")}>
                                <Text style={styles.btnText}>删除</Text>
                            </TouchableOpacity>
                        </View>
                        :
                        item.state == 1 ?
                            <View style={styles.btnWrap}>
                                <TouchableOpacity style={styles.btn} onPress={()=>pressAction(item._id,"nopass")}>
                                    <Text style={styles.btnText}>不通过</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.btn} onPress={()=>pressAction(item._id,"delete")}>
                                    <Text style={styles.btnText}>删除</Text>
                                </TouchableOpacity>
                            </View> :
                            <View style={styles.btnWrap}>
                                <TouchableOpacity style={styles.btn} onPress={()=>pressAction(item._id,"pass")}>
                                    <Text style={styles.btnText}>通过</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.btn} onPress={()=>pressAction(item._id,"delete")}>
                                    <Text style={styles.btnText}>删除</Text>
                                </TouchableOpacity>
                            </View>
                }
            </View>
        )
    }
    return (
        <View style={{ flex: 1 }}>
            <Text>Person!</Text>
            <Text>审核评论</Text>
            <FlatList
                data={blogModel.submitCommentArr}
                renderItem={_renderItem}
                style={{ flex: 1 }}
            />
        </View>
    )
})

const styles=StyleSheet.create({
    btnWrap:{
        flexDirection:"row",
    },
    btn:{
        width: 60,
        height: 30,
        backgroundColor: "grey",
        marginRight:10
    },
    btnText:{
        textAlign: "center",
        lineHeight: 30
    }
})