import { View, Text, FlatList, StyleSheet, TouchableOpacity, Image, ScrollView } from "react-native"
import { observer } from "mobx-react"
export const ListModel = observer((props) => {
    let {dataArr,goDetailAct,beLikeAct}=props
    const _renderItem = ({ item }) => {
        return (
            <View style={styles.wrap}>
                <Text style={styles.title} onPress={() => goDetailAct(item)}>{item.title}</Text>
                <Text numberOfLines={2} ellipsizeMode={'tail'} onPress={() => goDetailAct(item)}>{item.content}</Text>
                <View style={styles.foot}>
                    <TouchableOpacity style={styles.btn} onPress={() => beLikeAct(item, 'good')}>
                        <Image source={require('../imgs/good.png')} style={styles.imgs}></Image>
                        <Text style={styles.countsNum}>{item.good}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btn} onPress={() => beLikeAct(item, 'bad')}>
                        <Image source={require('../imgs/bad.png')} style={styles.imgs}></Image>
                        <Text style={styles.countsNum}>{item.bad}</Text>
                    </TouchableOpacity>
                    <Image source={require('../imgs/count.png')} style={styles.imgs}></Image>
                    <Text style={styles.countsNum} class="count">{item.count}</Text>
                </View>
                <View style={styles.bottom}>
                    <Text>分类：</Text>
                    <Text>{item.classify}</Text>
                    <Text style={styles.time}>{item.time}</Text>
                </View>
            </View>
        )
    }
    return (
        <FlatList
            data={JSON.parse(JSON.stringify(dataArr))}
            renderItem={_renderItem}
            keyExtractor={item => item._id}
            style={{ flex: 1 }}
        />
    )
})
const styles = StyleSheet.create({
    wrap: {
        marginBottom: 10
    },
    title: {
        fontSize: 20
    },
    foot: {
        marginLeft: 15,
        marginTop: 5,
        flexDirection: "row"
    },
    imgs: {
        width: 30,
        height: 30
    },
    btn: {
        marginRight: 25,
        flexDirection: "row"
    },
    countsNum: {
        textAlign: "center",
        lineHeight: 30,
        fontSize: 15
    },
    bottom: {
        marginLeft: 8,
        marginTop: 5,
        flexDirection: "row"
    },
    time: {
        position: "absolute",
        right: 0
    }
})