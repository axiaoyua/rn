import { View, Image, Text, useWindowDimensions } from 'react-native'
// 获取屏幕宽高：useWindowDimensions

export default function BottomIcon({ name }) {
    const windowWidth = useWindowDimensions().width;

    // 图片宽高自适应
    const imgWidth=windowWidth/12

    return (
        <View>
            <Text>
                {
                    name == 'homeAct' ?
                        <Image source={require('../imgs/homeAct.png')} style={{ width: imgWidth, height: imgWidth}}></Image> :
                        name == 'writeAct' ?
                            <Image source={require('../imgs/writeAct.png')} style={{ width: imgWidth, height: imgWidth }}></Image> :
                            name == 'HotAct' ?
                                <Image source={require('../imgs/hotAct.png')} style={{ width: imgWidth, height: imgWidth }}></Image> :
                                <Image source={require('../imgs/personAct.png')} style={{ width: imgWidth, height: imgWidth }}></Image>
                }
             </Text>

        </View>

    )
}