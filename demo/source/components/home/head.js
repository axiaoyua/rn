import {
    View, Text, Button, FlatList,
    TouchableOpacity, StyleSheet, useWindowDimensions
} from 'react-native'
import { observer } from 'mobx-react';
import { blogModel } from '../../../store/store'
import { useNavigation } from '@react-navigation/native';
import { CommonActions } from '@react-navigation/native';
import { useEffect, useState } from 'react';
export const HomeHead = observer(props => {
    const navigation = useNavigation();

    const windowWidth = useWindowDimensions().width;
    const btnWidth = windowWidth / 3

    useEffect(() => {
        // console.log("头部组件要更新啦！！！");
        blogModel.allclassify()
    }, [blogModel.blogsNumber])

    // 函数组件没有生命周期
    // 通过useEffect模拟componentDidMount
    // 当第二个参数为空数组时
    useEffect(() => {
        blogModel.allBlogs()
        blogModel.allclassify()
    }, [])

    const _renderItem = ({ item }) => {
        return (
            <View>
                <TouchableOpacity
                    style={[styles.btn, { width: btnWidth }]}
                    onPress={
                        () =>
                            navigation.dispatch(
                                CommonActions.navigate({
                                    name: "Classify",
                                    params: { classify: item },
                                })
                            )
                    }
                >
                    <Text style={styles.text}>{item}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    return (
        <View style={styles.container}>
            <FlatList
                data={blogModel.classifyArr}
                renderItem={_renderItem}
                style={styles.list}
            />
            <Text>home head!</Text>
        </View>
    )
})

const styles = StyleSheet.create({
    container: {
        flexWrap: "wrap"
    },
    list: {
        flexDirection: "row",
        flexWrap: "wrap"
    },
    btn: {
        height: 40,

    },
    text: {
        textAlign: "center",
        lineHeight: 40
    }
})