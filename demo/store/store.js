import { action, makeObservable, observable, computed } from 'mobx';
import axios from 'axios'
const moment = require('moment')

// 前端生成唯一id的函数
function uuid() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = Math.random() * 16 | 0,
          v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
  });
}

// 主页，分类列表页,文章详情页
export class Store {
  constructor() {
    makeObservable(this);
  }

  @observable allBlogArr = []
  @observable classifyArr = []

  @observable classifyListArr = []
  @observable sortBlogsArr = []
  @observable newBlogs = []


  @observable testArr = []

  @observable showCommentArr=[]   // 文章详情页要展示的评论列表
  @observable submitCommentArr=[]   // 管理页要展示的····
  @observable commentsNumber=0           // 每点击一次添加评论按钮，就会++
  @observable blogsNumber=0           // 每点击一次添加文章按钮，就会++

  // 文章详情页
  // 获取评论(根据文章id)
  @action 
  getComment(id){
    axios.post('http://localhost:9999/manage/getComment',{id}).then(res=>{
      this.showCommentArr=res.data
    })
  }
  // 添加评论。接收评论内容，文章标题，id.添加成功后++commentsNumber
  @action 
  addComment(content,title,id){
    let commentObj={
      content,
      time:moment().format('MMMM Do YYYY, h:mm:ss a'),
      articleTitle:title,
      articleId:id,
      state:0
    }
    axios.post('http://localhost:9999/manage/addComment',commentObj).then(res=>{
      ++this.commentsNumber
    })
  }

  // 管理员页
  // 获取全部提交的评论
  @action 
  getAllComments(){
    axios.post('http://localhost:9999/manage/getAllComments').then(res=>{
      this.submitCommentArr=res.data
    })
  }
  // 审核评论.评论的id，操作类型（通过，不通过，删除）
  @action 
  checkComment(id,type){  // pass,nopass,delete
    let newArr = this.submitCommentArr.concat([])
    if (type == 'pass') {
      for (let i = 0; i < newArr.length; i++) {
        if (newArr[i]._id == id) {
          newArr[i].state = 1
        }
      }
    } else if (type == 'nopass') {
      for (let i = 0; i < newArr.length; i++) {
        if (newArr[i]._id == id) {
          newArr[i].state = 2
        }
      }
    } else {
      for (let i = 0; i < newArr.length; i++) {
        if (newArr[i]._id == id) {
          newArr.splice(i,1)
        }
      }
    }
    this.submitCommentArr = newArr

    // 数据更新
    axios.post('http://localhost:9999/manage/checkComment',{id,type}).then(res=>{
    })
  }

  @action
  allBlogs() {
    axios.post("http://localhost:9999/").then(res => {
      this.allBlogArr = res.data
    }).catch(err => console.log(err))
  }

  @action
  allclassify() {
    axios.post("http://localhost:9999/allclassify").then(res => {
      this.classifyArr = res.data
    }).catch(err => console.log(err))
  }
  @action
  classifyList(classify) {
    axios.post("http://localhost:9999/classifyList", { classify }).then(res => {
      this.classifyListArr = res.data
    }).catch(err => console.log(err))
  }

  @action
  updataClick(number, id, typeStr) {
    let newArr = this.classifyListArr.concat([])

    if (typeStr == 'count') {
      for (let i = 0; i < newArr.length; i++) {
        if (newArr[i]._id == id) {
          newArr[i].count = number
        }
      }
    } else if (typeStr == 'good') {
      for (let i = 0; i < newArr.length; i++) {
        if (newArr[i]._id == id) {
          newArr[i].good = number
        }
      }
    } else {
      for (let i = 0; i < newArr.length; i++) {
        if (newArr[i]._id == id) {
          newArr[i].bad = number
        }
      }
    }
    this.classifyListArr = newArr
    axios.post("http://localhost:9999/updataclick", { number, id, typeStr }).then(res => {
    }).catch(err => console.log(err))
  }
  @action
  updataHotClick(number, id, typeStr) {
    let newArr = this.sortBlogsArr.concat([])

    if (typeStr == 'count') {
      for (let i = 0; i < newArr.length; i++) {
        if (newArr[i]._id == id) {
          newArr[i].count = number
        }
      }
    } else if (typeStr == 'good') {
      for (let i = 0; i < newArr.length; i++) {
        if (newArr[i]._id == id) {
          newArr[i].good = number
        }
      }
    } else {
      for (let i = 0; i < newArr.length; i++) {
        if (newArr[i]._id == id) {
          newArr[i].bad = number
        }
      }
    }
    this.sortBlogsArr = newArr
    axios.post("http://localhost:9999/updataclick", { number, id, typeStr }).then(res => {
    }).catch(err => console.log(err))
  }
  @action
  updataWriteClick(number, id,time, typeStr) {
    let newArr = this.newBlogs.concat([])

    if (typeStr == 'count') {
      for (let i = 0; i < newArr.length; i++) {
        if (newArr[i]._id == id) {
          newArr[i].count = number
        }
      }
    } else if (typeStr == 'good') {
      for (let i = 0; i < newArr.length; i++) {
        if (newArr[i]._id == id) {
          newArr[i].good = number
        }
      }
    } else {
      for (let i = 0; i < newArr.length; i++) {
        if (newArr[i]._id == id) {
          newArr[i].bad = number
        }
      }
    }
    this.newBlogs = newArr
    axios.post("http://localhost:9999/updatawriteclick", { number, time, typeStr }).then(res => {
    }).catch(err => console.log(err))
  }
  @action
  // 调用action时，要传递id作为参数.
  // 同时未来区分是哪个地方跳转至详情页，要给它再传一个参数from
  pageDetail(id, from) {
    // 为了区分是从哪里跳转过来的，在调用action时传了参数：from
    if (from == 'classifyList') {
      let arr = JSON.parse(JSON.stringify(this.classifyListArr))
      for (let i = 0; i < arr.length; i++) {
        if (arr[i]._id == id) {
          return i
        }
      }
    } else if (from == 'Hot') {
      let arr = JSON.parse(JSON.stringify(this.sortBlogsArr))
      for (let i = 0; i < arr.length; i++) {
        if (arr[i]._id == id) {
          return i
        }
      }
    } else {
      let arr = JSON.parse(JSON.stringify(this.newBlogs))
      for (let i = 0; i < arr.length; i++) {
        if (arr[i]._id == id) {
          return i
        }
      }
    }
  }

  @action
  sortBlogs() {
    axios.post('http://localhost:9999/allSortBlogs').then(res => {
      this.sortBlogsArr = res.data
    })
  }

  @action
  addBlog(title, content, classify) {
    let newBlog = {
      title,
      content,
      time: moment().format('MMMM Do YYYY, h:mm:ss a'),
      count: 0,
      good: 0,
      bad: 0,
      classify: classify.toLowerCase()
    }
    this.newBlogs.push({...newBlog,_id:uuid()})
    axios.post('http://localhost:9999/addNewBlogs', newBlog).then(res => {
      ++this.blogsNumber
    })
  }
}

export const blogModel = new Store();