const express = require('express')
const CommentsModel = require('../models/comment')
const router = express.Router()

// 获取评论，审核评论
router.post('/getComment', (req, res) => {
    let {id}=req.body
    CommentsModel.find({articleId:id,state:1}).then(data=>{
        res.send(data)
    })
})
// 添加评论
router.post('/addComment', (req, res) => {
    let newComment=new CommentsModel(req.body)
    newComment.save().then(data=>{
        res.send(data)
    })
})

// 管理员页
// 获取全部提交的评论
router.post('/getAllComments', (req, res) => {
    CommentsModel.find({}).then(data=>{
        res.send(data)
    })
})
// 审核评论
router.post('/checkComment', (req, res) => {
    let {id,type}=req.body
    if(type=='pass'){
        CommentsModel.updateOne({_id:id},{state:1}).then(data=>{
            res.send(data)
        })
    }else if(type=='nopass'){
        CommentsModel.updateOne({_id:id},{state:2}).then(data=>{
            res.send(data)
        })
    }else{
        CommentsModel.remove({_id:id}).then(data=>{
            res.send(data)
        })
    }
})
module.exports = router