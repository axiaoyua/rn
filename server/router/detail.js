const express = require('express')

const router = express.Router()

const PagesModel=require('../models/pages')

// 文章详情页
router.post('/page',(req,res)=>{
    let {id}=req.body
    PagesModel.find({_id:id}).then(data=>{
        res.send(data)
    })
})

router.post('/pageChange',(req,res)=>{
    let {id,number,type}=req.body
    if(type=='good'){
        PagesModel.updateOne({_id:id},{good:number}).then(data=>{
        })
    }else{
        PagesModel.updateOne({_id:id},{bad:number}).then(data=>{
        })
    }
})
module.exports = router