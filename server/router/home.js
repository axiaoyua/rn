const express = require('express')

const router = express.Router()

const PagesModel=require('../models/pages')

// 主页，分类列表页
router.post('/',(req,res)=>{
    PagesModel.find().then(data=>{
        res.send(data)
    })
})

router.post('/allclassify',(req,res)=>{
    PagesModel.find().then(data=>{
        let classifyArr=[]
        for(let i=0;i<data.length;i++){
            classifyArr.push(data[i].classify)
        }
        classifyArr=Array.from(new Set(classifyArr))
        res.send(classifyArr)
    })
})
router.post('/classifyList',(req,res)=>{
    let {classify}=req.body
    PagesModel.find(classify).then(data=>{
        res.send(data)
    })
})
router.post('/updataclick',(req,res)=>{
    let {number,id,typeStr}=req.body
    if(typeStr=='count'){
        PagesModel.updateOne({_id:id},{count:number}).then(res=>{
            PagesModel.find({_id:id}).then(data=>{
            })
        })
    }else if(typeStr=='good'){
        PagesModel.updateOne({_id:id},{good:number}).then(res=>{
            PagesModel.find({_id:id}).then(data=>{
            })
        })
    }else{
        PagesModel.updateOne({_id:id},{bad:number}).then(res=>{
            PagesModel.find({_id:id}).then(data=>{
            })
        })
    }
    
    
})

router.post('/updatawriteclick',(req,res)=>{
    let {number,time,typeStr}=req.body
    if(typeStr=='count'){
        PagesModel.updateOne({time:time},{count:number}).then(res=>{
            PagesModel.find({time:time}).then(data=>{
            })
        })
    }else if(typeStr=='good'){
        PagesModel.updateOne({time:time},{good:number}).then(res=>{
            PagesModel.find({time:time}).then(data=>{
            })
        })
    }else{
        PagesModel.updateOne({time:time},{bad:number}).then(res=>{
            PagesModel.find({time:time}).then(data=>{
            })
        })
    }
    
    
})

router.post('/allSortBlogs',(req,res)=>{
    PagesModel.find({}).sort({count:-1}).then(data=>{
        res.send(data)
    })
})

router.post('/addNewBlogs',(req,res)=>{
    let newPage=req.body
    const NewPage=new PagesModel(newPage)
    NewPage.save().then(data=>{
        res.send(data)
    })
})
module.exports = router